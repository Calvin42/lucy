import os
import sys
import json
import pprint

#return a data file with json content and filenode
def openJson(nome, mode):
	fileName = nome+".json"
	jsonData = open(fileName,  mode)
	res = {"json":json.load(jsonData), "file": jsonData}
	return res

def checkForStat(person):
	data = openJson("stats", "r+")
	# print("CHECK")
	# pprint.pprint (data)
	for el in data["json"]:
		pprint.pprint(el)
		if el["person"] == person:
			data["file"].close()
			return True
	data["file"].close()
	return False

def addStat(nome, person):
	data = openJson("stats", "w+")
	data["json"][person] = 1
	json.dump(data["json"], data["file"], indent = 4)
	data["file"].close()

def updateStats( person):
	data = openJson("stats", "r+")
	print("UPDATE")
	pprint.pprint(data)
	for el in data["json"]:
		if el["person"] == person:
			el["count"] += 1
	data["file"].seek(0)
	json.dump(data["json"], data["file"], indent=4)
	data["file"].close()
