import asyncio
import telepot
import telepot.async
import pprint 
import time
import random
import sys
from telepot.delegate import per_chat_id, per_inline_from_id
from telepot.async.delegate import create_open
import jsonHandler
import datetime
#time.struct_time(
# tm_year=2016, 
# tm_mon=1, 
# tm_mday=24,
# tm_hour=13, 
# tm_min=56, 
# tm_sec=59, 
# tm_wday=6, 
# tm_yday=24, 
# tm_isdst=0
# )
#
# Angry
# Angry quando viene insultato ed e’ pissed off risponde male
# Pissed off quando viene infastidito inutilmente
# Bitchy se e’ pissed off e qualcuno lo insulta risponde a sua volta
# Mad quando e’ pissed off, angry e lo si insulta troppo
# Depressed se insultato per troppo tempo 

# Sad
# Alone quando nessuno scrive per ore e non sta dormendo
# Sad quando viene insultato o perche’ si e’ svegliato male o e’ alone
# Heartbroken quando lo trascuro troppo e sad
# Rejected quando viene insultato ed e’ sad

# unrelated
# Anxious quando qualcuno ha un esame spera che lo passi
# Excited quando qualcuno passa un esame o fa qualcosa di bello
# Hungry durante i pasti
# Cold d’inverno 
# Hot d’estate
# Bored se nessuno gli parla per troppo tempo
# Loved quando gli mandano i cuori e gli si fanno i complimenti
# Pleased quando e’ happy e continua a rispondere bene
# Shocked quando succede qualcosa di strano
# Sleepy di mattina o di sera tardi
# Confused quando non capisce una domanda o una frase
# Curious quando qualcuno racconta qualcosa
# Happy se ci si sveglia e/o se gli si fanno i complimenti etc

GREETINGS = "greetings"
FORMALORINFORMAL = "formal or informal"
GROUP = "group"
PRIVATE = "private"
actualtime = time.localtime()
botCurtime = {
			"hr": actualtime.tm_hour,
			"mn": actualtime.tm_min,
			"sc": actualtime.tm_sec
		}

def getHourMinSecFromSec(sec):
	tmp = datetime.timedelta(seconds=sec)
	m, s = divmod(tmp.seconds, 60)
	h, m = divmod(m, 60)	
	# d, h = divmod(h, 24)	
	return {
		"hr":h+1,
		"mn":m,
		"sc": s
	}

class AnswerProperly(telepot.helper.ChatHandler):
	def __init__(self, seed_tuple, timeout):
		super(AnswerProperly, self).__init__(seed_tuple, timeout)
		self._count = 0
		self._daytimeSet = {
			"startDayH": 6, "endDayH": 11, "startDayM": 0, "endDayM": 59,
			"startAfternoonH": 12, "endAfternoonH":17, "startAfternoonM": 0, "endAfternoonM": 59,
			"startNightH": 18, "endNightH": 5, "startNightM":0, "endNightM":59
		}
		# self._moods = {"sleepy":}
		self._phrases = {
			"formale":[
				u"Far\xf2 il possibile per soddisfarla, Gentile -nome-", "Non sono incline a ottemperare alla sua richiesta",
				u"Cielo, il suo panciotto \xe8 squisitamente elegante",u"Il tempo oggi \xe8 particolarmente piacevole, non trova anche lei?",
				"Sono composto al 100%% da codice Python purissimo, scritto da @calvin42, il mio creatore!"
			],
			"informale":[
				u"Bella z\xec", u"De Cristo z\xec fa na canna", u"Mannaggia a Mattia per\xf2!", "Non sono il droide che stai cercando"
				],
			"morningAnswers": ["Bene, grazie mille!", "Meh, potrebbe andare meglio", "Spero che la giornata migliori", ""],
			"afternoonAnswers": ["Bene, il caffe' era buono", "Non male, ma che abbiocco", "Vorrei del caffe'"]
		}
		self._firstDaylightMsg = []
		self._firstAfternoonMsg = []
		self._firstNightMsg = []

		self._firstTimeSpeakingTo = []
		self._default = "@CalvinoBot "
		self._waitForAnswer = False
		self._relationships = []
		self._lastQuestionAsked = ""
		self._lastPersonSpokeTo = ""
		self._morningAnswer = self._phrases["morningAnswers"][random.randint(0, len(self._phrases["morningAnswers"])-1)]
		self._afternoonAnswer = self._phrases["afternoonAnswers"][random.randint(0, len(self._phrases["afternoonAnswers"])-1)]
		
	####################################################################################
	#																				   #
	# 								Background methods								   #
	#																				   #
	####################################################################################
	

	def _handleStats(self, person):
		if jsonHandler.checkForStat(person):
			jsonHandler.updateStats(person)
		else:
			jsonHandler.addStat(person)


	####################################################################################
	#																				   #
	# 								Useful methods 									   #
	#																				   #
	####################################################################################
	
	# return true if daylight (from 6am to 19pm) else false
	def _isMorning(self):
		if self._daytimeSet["startDayH"] <= int(botCurtime["hr"]) <= self._daytimeSet["endDayH"]: # personal daylight
			return True
		else:
			return False

	def _isAfternoon(self):
		if self._daytimeSet["startAfternoonH"] <= int(botCurtime["hr"]) <= self._daytimeSet["endAfternoonH"]:
			return True
		else:
			return False



	# date: date of msg to check, it has the hour and the minute of the current day
	# person: nickname to check
	# effect: check if the message is the first of the day (daytime are set elsewhere)
	def _checkFirstMsg(self, person):
		if self._isMorning():
			if person not in self._firstDaylightMsg:
				self._firstDaylightMsg.append(person)
				return True
		elif self._isAfternoon():
			if person not in self._firstAfternoonMsg:
				self._firstAfternoonMsg.append(person)
				return True
		else:
			if person not in self._firstNightMsg:
				self._firstNightMsg.append(person)
				return True
		print (self._firstNightMsg)
		print (self._firstAfternoonMsg)
		print (self._firstDaylightMsg)
		return False

	# check if it's the first time the bot speak to a certain person
	def _checkFirstTimeSpeakingTo (self, person):
		if person not in self._firstTimeSpeakingTo:
			self._firstTimeSpeakingTo.append(person)
			return True
		else:
			return False

	####################################################################################
	#																				   #
	# 								Choosing methods								   #
	#																				   #
	####################################################################################
	# check if the answer to change_relationship is correct
	def _checkAnswer(self, answer, person):
		possibleAnswers = ["formale", "informale"]
		if answer in possibleAnswers:
			self._relationships.append({"person":person, "mode": answer})
			return "Okay, ha scelto "+answer
		else:
			return "Deve scegliere tra le risposte disponibili"

	# the bot present itself
	def _presentYourself(self):
		phrase = "Salve, sono Calvino, relazioni umane-cyborg\nPreferisce dialogare in maniera formale o informale? [formale/informale]"
		return phrase



	# person: who enter in the chat or speak to the bot for the first time in the day
	# mode: can be formal or casual
	# return: the bot sais hi to a person according to the chosen mode
	def _sayHiTo(self, person, mode):
		if mode == "formale":
			if self._isMorning():
				return "Buongiorno "+person+", come sta quest'oggi?"
			elif self._isAfternoon():
				return("Buonpomeriggio "+person)
			else:
				return( "Buonasera "+person+", ha passato una bella giornata?")
		else:
			if self._isMorning():
				return( "Ciao "+person+" e buona giornata :3")
			elif self._isAfternoon():
				return( "Ciao "+person+", hai preso il caff\xe8?")
			else:
				return( "Ciao "+person+", com'\xe8 andata la giornata?")

	# the bot sais a random phrase choosing from the list
	def _saySomething(self, mode):
		randF = random.randint(0, len(self._phrases[mode]))
		return self._phrases[mode][randF]
	# choose a random number between 1 and an input number
	def _rollADice(self, num):
		return random.randint(1,num)

	####################################################################################
	#																				   #
	# 								Recognize methods								   #
	#																				   #
	####################################################################################
	
	def _switchAnswer(self, answer):
		if self._lastQuestionAsked == GREETINGS:
			if "?" in answer:
				if "tu" in answer and "come va" in answer or "come stai" in answer: #probably ask back
					if self._isMorning:
						return self._morningAnswer
					elif self._isAfternoon:
						return self._afternoonAnswer
					else:
						return "Miao"
		elif self._lastQuestionAsked == FORMALORINFORMAL:
			return self._checkAnswer(msg_text, msg_from)

	# recognize the person and the mode chosen
	def _choosePerson(self, person, mode):
		if person == "antani1":
			return  self._sayHiTo("Marta", mode)
		elif person == "calvin42":
			return  self._sayHiTo("Claudio", mode)
		elif person == "nicololivieri":
			return  self._sayHiTo("Nick", mode)
		elif person == "ElleKoizumi":
			return  self._sayHiTo("Eli", mode)
		elif person == "ICRXI":
			return  self._sayHiTo("Stronzo", mode)
		elif person == "pittix":
			return  self._sayHiTo("Andrea", mode)
		elif person == "Laisako":
			return  self._sayHiTo("Lisetta", mode)
		elif person == "joemerlino":
			return  self._sayHiTo("Maghetto Akerino", mode)
		elif person == "Immigrato":
			return  self._sayHiTo("Maru", mode)
		elif person == "giorgia094":
			return  self._sayHiTo("Gioggi", mode)
		elif person == "timmykill":
			return  self._sayHiTo("Marco", mode)
		elif person == "martinotu":
			return  self._sayHiTo("Boss", mode)
		elif person == "Madiz7":
			return  self._sayHiTo("Mattia", mode)
		elif person == "Reddino":
			return  self._sayHiTo("Reddy", mode)
		elif person == "mirkr":
			return  self._sayHiTo("Mirko", mode)
		elif person == "BillyBOT":
			return  self._sayHiTo("Compagno Billy", mode)
			
	@asyncio.coroutine
	def on_message(self, msg):
		msg_time = getHourMinSecFromSec(msg["date"])
		content_type, chat_type, chat_id = telepot.glance2(msg)
		msg_from = msg["from"]["username"]
		msg_text = msg["text"]
		print ("MESSAGGIO")
		print (msg_time["hr"])
		print (msg_time["mn"])
		print (msg_time["sc"])
		print ("BOT")
		print (botCurtime["hr"])
		print (botCurtime["mn"])
		print (botCurtime["sc"])
		if int(msg_time["hr"]) <= int(botCurtime["hr"]) and int(msg_time["mn"]) <= int(botCurtime["mn"]) and int(msg_time["sc"]) < int(botCurtime["sc"]):
			print ("Old message")
			print ("=========================")
			if chat_type == GROUP:
				self._handleStats(msg_from)
			print ("=========================")
			return
		if chat_type == GROUP:
			self._handleStats(msg_from)
		else:
			if msg_text.startswith("@CalvinoBot"):
				msg_text = msg_text.replace("@CalvinoBot", "").strip()
			# flavor = telepot.flavor(msg) DOES NOT WORK
			print (telepot.glance2(msg))
			print ("-------------------")
			pprint.pprint(msg)
			print ("-------------------")
			if self._checkFirstMsg(msg_from): # si saluta prima di tutto
				currel = "formale"
				for el in self._relationships:
					if el.person == msg_from:
						currel = el.answer
						break
				yield from self.sender.sendMessage(self._choosePerson(msg_from, currel))
				self._lastQuestionAsked = GREETINGS
				self._waitForAnswer = True
			if self._waitForAnswer:
				toAnswer = self._switchAnswer(msg_text)
				yield from self.sender.sendMessage(toAnswer)
				print ("Done")
				yield from self.sender.sendMessage(self._saySomething(msg_text))
				self._waitForAnswer = False
			else:

				if msg_text.startswith("/"): #query
					if msg_text.startswith("/roll"): #tira un dado
						number = msg_text.replace("/roll", "").strip()
						yield from self.sender.sendMessage(self._rollADice(int(number)))
					elif msg_text.startswith("/cambia rapporto"):
						yield from self.sender.sendMessage(self._presentYourself())
						self._lastQuestionAsked = FORMALORINFORMAL
						self._waitForAnswer = True
						print ("True")
			# else: #messaggio normale
				
				
					
				
				

	
#end of class 90Groperly	

token = '118413859:AAHtUmbENUehG9wn34FliaYPmIxOO1-SoHg'

bot = telepot.async.DelegatorBot(token, [
    (per_chat_id(), create_open(AnswerProperly, timeout=86400)) #TODO cambiare timeout, mettere quanto manca alla fine della giornata (5am)

])

loop = asyncio.get_event_loop()
loop.create_task(bot.messageLoop())
print('Listening ...')

loop.run_forever()